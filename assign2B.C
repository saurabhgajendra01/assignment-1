#include <stdio.h>
#include <string.h>

int main () {
   char str1[50], str2[50];

   //destination string \\string 
   strcpy(str1, "initial string");

   //source string
   strcpy(str2, ", add this");

   //concatenating the string str2 to the string str1
   strcat(str1, str2);

   //display
   printf("String after concatenation: %s", str1);

   return(0);
}
