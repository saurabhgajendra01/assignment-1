#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define PRODUCT_DB "product.db"
#define TEMP_DB  "item.db"

typedef struct product
{
    int id;
    char name[20];
    int price;
    int qty;
}product_t;


// accept data
int get_next_id()
{
    FILE *fp;
    int max=0;
    int size = sizeof(product_t);
    product_t p;
    //open the file
    fp=fopen(PRODUCT_DB,"rb");
    if(fp==NULL)
    {
        return max + 1;
    }
    //change file pos to last record
    fseek(fp,-size,SEEK_END);
    //read the record from the file
    if(fread(&p,size,1,fp)>0)
      //if read is successful get max (its) 
       max =p.id;
       //close the file
       fclose(fp);
       return max + 1;
}

void product_accept(product_t *p)
{
    p->id = get_next_id();
    printf("\nproduct name: ");
    scanf("%s", p->name);     
    printf("\nQuantity: ");
    scanf("%d", &p->qty);
    printf("\nprice: ");
    scanf("%d",&p->price);
}

void product_display(product_t *p)
{
      printf("\nID: %d\nname: %s\nquantity: %d\nprice: %d\n",p->id,p->name,p->qty,p->price);
}

// add record in file
void adddata()
{    
    FILE *fp;

    product_t p;
    //accept details of products
    product_accept(&p);
    //add product in file
    //open file
    fp=fopen(PRODUCT_DB,"ab");
    if(fp==NULL)
    {
        printf("failed to open users file!!");
        return;
    }
    //write data into file
    fwrite(&p,sizeof(product_t),1,fp);

    fclose(fp);  //close the file        
    
}

// display records
void display_all()
{
    FILE *fp;
    product_t p;
    int count=0;
    //open user file
    fp= fopen(PRODUCT_DB,"rb");
    if(fp == NULL) 
    {
		perror("cannot open file");
		exit(1);
	}
    //read one by one 
   
     while(fread(&p,sizeof(product_t),1,fp)>0)
    {
        product_display(&p);
        count++;
    }
    
    //close file
    fclose(fp);
     if(count==0)
         printf("No record present.");
}


void find_product_by_name(char name[])
{
    FILE *fp;
    int found=0;
    product_t p;
    //open the file for reading
    fp=fopen(PRODUCT_DB,"rb");
     if(fp==NULL)
    {
        perror("failed to open file!!!");
        return;
    }

    //read all records
    while(fread(&p,sizeof(product_t),1,fp)>0)
    {
        // if book name is matching, found 1
        if(strstr(p.name,name)!= NULL)
        {
            found=1;
            product_display(&p);
        }
    }

    //close file
    fclose(fp);
    if(!found)
       printf("No such record found.\n");
}

void product_edit_by_id()
{
    int id, found = 0;
	FILE *fp;
	product_t p;
	// input product id from user.
	printf("enter product id to be search: ");
	scanf("%d", &id);
	// open file
	fp = fopen(PRODUCT_DB, "rb+");
	if(fp == NULL)
    {
		perror("cannot open file");
		exit(1);
	}
	// read record one by one and check if product with given id is found.
	while(fread(&p, sizeof(product_t), 1, fp) > 0) 
    {
		if(id == p.id) 
		{
			found = 1;
			break;
		}
	}
	// if found
	if(found) 
	{
		// input new details from user
		long size = sizeof(product_t);
		product_t np;
	    product_accept(&np);
		np.id = p.id;
		// take file position one record behind.
		fseek(fp, -size, SEEK_CUR);
		// overwrite product details into the file
		fwrite(&np, size, 1, fp);
		printf("product updated.\n");
	}
	else // if not found
		// show message to user that product not found.
		printf("product not found.\n");
	// close file
	fclose(fp);
}

void del_data()
{
  
    int id, found=0;
    product_t p;
     // input item id to be deleted
    printf("Enter item ID : ");
    scanf("%d",&id);   

    FILE *fp;
    FILE *tmp;
    //open file
    fp=fopen(PRODUCT_DB,"rb");
    tmp=fopen(TEMP_DB,"wb");
     if(fp==NULL)
    {
        perror("failed to open items file");
        return;
    }

     if(tmp==NULL)
    {
        perror("failed to open temp file");
        return;
    }

    //read items 1 by 1

    while(fread(&p,sizeof(product_t),1,fp)>0)
    {
       
        if(id==p.id)
        {
            found = 1;
            printf("Item %d is deleted from inventory",p.id);
              
        }
        else
        {
            fwrite(&p, sizeof(product_t),1,tmp);
        }
        
    }
    if(!found)
    {
        printf("\n No such item found \n");

    }
            
        
    fclose(fp);
    fclose(tmp);
    remove(PRODUCT_DB);
    rename(TEMP_DB,PRODUCT_DB);


}


int main()
{
    int ch;
    char n[10];

    do
	{
		printf("\n 0. To exit");
        printf("\n 1. To add data");
        printf("\n 2. To view the records");
        printf("\n 3. To edit data");
        printf("\n 4. To search  data");
        printf("\n 5. To delete a data");

        printf("\n Enter your choice: ");
        scanf("%d",&ch);

         switch(ch)
         {
            case 0:
                    exit(0);
                    break;

            case 1:
                    adddata();
                    break;

            case 2: 
                    display_all();
                    break;

            case 3: 
                    product_edit_by_id();
                    break;
            
            case 4: 
                    printf("Enter product name to be search: ");
                    scanf("%s",n);
                    find_product_by_name(n);
                    break;

            case 5:
                   del_data();
                  
                    break;
        
        }

    }while(ch!=0);

    return 0;
}