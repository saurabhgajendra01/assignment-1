            
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MOVIE_DB	"movies.csv"

typedef struct movie {
	int id;
	char name[80];
	char genres[220];
}movie_t;

void movie_display(movie_t *m) {
	printf("id=%d, name=%s, genres=%s\n", m->id, m->name, m->genres);
}

typedef struct node {
	movie_t data;
	struct node *next;
}node_t;

node_t *head = NULL; // address of the first node of list (initially NULL) -- global

node_t* create_node(movie_t val) {
	node_t *newnode = (node_t*)malloc(sizeof(node_t));
	newnode->data = val;
	newnode->next = NULL;
	return newnode;
}

void add_last(movie_t val) {
	node_t *newnode = create_node(val);
	if(head == NULL)
		head = newnode;
	else {
		node_t *trav = head;
		while(trav->next != NULL) {
			trav = trav->next;
		}
		trav->next = newnode;
	}
}

void display_list() {
	node_t *trav = head;
	while(trav != NULL) {
		movie_display(&trav->data);
		trav = trav->next;
	}
}

int parse_movie(char line[], movie_t *m) {
	int success = 0;
	char *id, *name, *genres;
	id = strtok(line, ",\n");
	name = strtok(NULL, ",\n");
	genres = strtok(NULL, ",\n");
	if(id == NULL || name == NULL || genres == NULL)
		success = 0; // partial record
	else {
		success = 1;
		m->id = atoi(id);	// "10" (string) --> 10 (int)
		strcpy(m->name, name);
		strcpy(m->genres, genres);
	}
	return success;
}

void load_movies() {
	FILE *fp;
	char line[1024];
	movie_t m;
	fp = fopen(MOVIE_DB, "r");
	if(fp == NULL) {
		perror("failed to open movies file");
		exit(1);
	}

	while(fgets(line, sizeof(line), fp) != NULL) {
		//printf("%s\n", line);
		parse_movie(line, &m);
		//movie_display(&m);
		add_last(m); // add movie object into the linked list
	}

	fclose(fp);
}

void find_movie_by_name() {
	char name[80];
	node_t *trav = head;
	int found = 0;
	printf("enter movie name to be searched: ");
	gets(name);

	trav = head;
	while(trav != NULL) {
		if(strcmp(name, trav->data.name) == 0) {
			movie_display(&trav->data);
			found = 1;
			break;
		}
		trav = trav->next;
	}

	if(!found)
		printf("movie not found.\n");
}

void find_movie_by_genre() {
	char genre[80];
	node_t *trav = head;
	int found = 0;
	printf("enter movie genre to be searched: ");
	gets(genre);

	trav = head;
	while(trav != NULL) {
		if(strstr(trav->data.genres, genre) != NULL) {
			movie_display(&trav->data);
			found = 1;
		}
		trav = trav->next;
	}

	if(!found)
		printf("movie not found.\n");
}

int main() {
	load_movies();
	display_list(); // diplay movie linked list
	find_movie_by_name();
	find_movie_by_genre();
	return 0;
}

// Implentation steps
//	1. read file line by line.
//		a. open the file -- fopen().
//		b. read line one by one until eof -- fgets().
//		c. close the file -- fclose().
//	2. split each line by comma and load data into movie struct object.
//		line = "1,Toy Story (1995),Adventure|Animation|Children|Comedy|Fantasy"
//		delimiter = ",\n"
//		char *id = strtok(line, ",\n");
//		char *name = strtok(NULL, ",\n");
//		char *genres = strtok(NULL, ",\n");
//	3. add movie objects into linked list.
//		
//	4. find movie by name/genre from linked list.

// terminal> gcc assign8.c -o assign8.exe
// terminal> assign8.exe

// token = strtok(string, delim) - split string by given delimiters and return pointer to the token.
// example string: "this,is,a,string"
//	ptr = strtok(string, ",") -- returns pointer to the first token.
//		(1) ptr = "this" (address of "this")
//	ptr = strtok(NULL, ",") -- returns sub-sequent tokens on each call.
//							-- returns NULL when all tokens are completed.
//		(2) ptr = "is" (address of "is")
//		(3) ptr = "a" (address of "a")
//		(4) ptr = "string" (address of "string")
//		(5) ptr = NULL

// Linked list is list of records linked together. Each record hold data and pointer to the next record.
// (head)[*]--->[data|*]--->[data|*]--->[data|*]--->[data|*]--->[data|*]--->[data|*]--->0(NULL)
// Each item in linked list is called as "Node".
//	Node has "data"	and pointer to "next" Node.
//		struct node {
//			data-type data;
//			struct node *next;	
//		};
// Linked list can grow/shrink dynamically (unlike arrays -- fixed size).
//	Each node must be allocated dynamically using malloc function(in C) or new operator(in C++/Java).
// Address of first node of linked list is kept in a special pointer - called as "head" pointer.
//	To traverse the linked list, we always start from head pointer and access all nodes one by one (sequentially).
// Operations on Linked List:
//	1. add node at the beginning
//	2. add node at the end **
//	3. add node at given position
//	4. delete node at the beginning
//	5. delete node at the end
//	6. delete node at given position
//	7. display all nodes (data) ** 
//	8. find the node ** 
//	9. find the node and delete it
//	...

            
